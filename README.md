# JSONObject

Serializes JSON into pre-defined PHP classes.

## Description

JSONObject adds the ability to design classes around JSON structure and not have to worry about the hassle of JSON parsing. The functionality was heavily inspired by PDO's FETCH_CLASS fetch mode.

## Getting Started

### Dependencies

* Requires PHP >=7.4
* Requires PHP JSON extension. Starting in PHP 8, this is a core extension

### Installing

Coming soon.

### Example

Given the following JSON document, stored in driver PHP variable `$json`:

```json
[
    {
        "username": "jdoe",
        "age": 30,
        "favorite_foods": [
            "Ice Cream",
            "Salad"
        ],
        "contact": {
            "email": "jdoe@example.com"
        }
    },
    {
        "username": "jsmith",
        "age": 35,
        "favorite_foods": [
            "Peanut Butter & Jelly",
            "Steak"
        ],
        "contact": {
            "email": "jsmith@example.com"
        }
    }
]
```

and the following PHP classes:
```php

class UserStore {
    /**
     * PHP 7.4 compatibility
     * @ArrayOf User
     * @JSONRoot true
     */
    #[ArrayOf('User')]
    #[JSONObjectProperty(root: true)
    private array $users;
    
    public function getUsers(): array
    {
        return $this->users;
    }
}

class User {
    private string $username;
    private int $age;
    /** 
     * Tells the parser that your variable name does not match the field name.
     * @JSONProperty favorite_foods 
     */
    #[JSONPropertyObject(field: 'favorite_foods')
    private array $favoriteFoods;
    private ContactInformation $contact;
    
    public function getUsername(): string
    {
        return $this->username;
    }
    
    public function getFavoriteFoods(): array
    {
        return $this->favoriteFoods;
    }
}

class ContactInformation {
    private string $email;
}
```

You are able to retrieve a fully populated `UserStore` by using the following code in your driver:

```php
use Arinity\JSONObject\JSONObject;
    
// Note: $json is the JSON string we stored earlier.
$userStore = JSONObject::parse($json, 'UserStore');
    
// Print out each users favorite foods
foreach ($userStore->getUsers() as $user) {
    $favoriteFoods = $user->getFavoriteFoods();
    if (count($favoriteFoods) >= 1) {
        printf(
            "%s's favorite foods are: %s\n", 
            $user->getUsername(), 
            implode(', ', $favoriteFoods)
        );
    } else {
        printf("%s has no favorite foods :/\n", $user->getUsername());
    }
}
```

## Questions

Please [open an issue](https://gitlab.com/arinity/php-json-object/-/issues/new?issue%5Bmilestone_id%5D=) if you need any assistance.
