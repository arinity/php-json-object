<?php

namespace Arinity\JSONObject\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class JSONObjectProperty {
    public function __construct(
        public ?bool $root = false,
        public ?string $arrayAsClass = null,
        public ?string $field = null
    ) {
    }
}
